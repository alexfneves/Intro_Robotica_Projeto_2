clear;

q0 = [0 pi/2 -pi pi 0 0]';

%             th    d       a         alpha      type  offset    mdh
L(1) = Link([ 0     0       0         pi/2        0     0],  'standard');
L(2) = Link([ 0 	0     0.2794	   0          0     0],  'standard');
L(3) = Link([ 0     0	    0        -pi/2        0     0],  'standard');
L(4) = Link([ 0   0.2286    0         pi/2        0     0],  'standard');
L(5) = Link([ 0     0       0        -pi/2        0     0],  'standard');
L(6) = Link([ 0     0	    0          0          0     0],  'standard');

antro = SerialLink(L, 'name', 'Antro (6R)', 'comment', 'Alex Fernandes Neves');

save_figure = true;
plot_figures = true;

if plot_figures
    figure(1);
    close(1);
    antro.plot(q0', 'floorlevel', -0.1, 'noname', 'notiles');
    if save_figure
        export_fig(['latex/figs/ex1_ready'], '-pdf', '-painters', '-transparent');
    end
end

wn = 2*pi/10;
syms t real;

% trajetória a
xda = 0.001*[75*sin(wn*t) + 228 0 75*cos(wn*t) + 278]';
dxda = diff(xda, t);
matlabFunction(xda, 'File', 'xda');
matlabFunction(dxda,  'File', 'dxda');

% trajetória b
n = [1 -1 1]';
n = n/norm(n);
p = [0.228 0 0.278]';
xdb = xda - (xda - p)'*n*n;
dxdb = diff(xdb, t);
matlabFunction(xdb, 'File', 'xdb');
matlabFunction(dxdb,  'File', 'dxdb');

% trajetória c
xdc = 0.001*[(75*(sin(wn*t) + sin(4*wn*t)) + 228) 0 (75*(cos(wn*t) + cos(4*wn*t)) + 69)]';
dxdc = diff(xdc, t);
matlabFunction(xdc, 'File', 'xdc');
matlabFunction(dxdc,  'File', 'dxdc');


%%

for i = 1:3 % trajetórias a,b e c
    for j = 1:3 % controle 1, 2a e 2b
        traj_num = i;
        switch traj_num
            case 1 % trajetória a
            traj_s = 'a_';
            case 2 % trajetória b
            traj_s = 'b_';
            case 3 % trajetória c
            traj_s = 'c_';
        end
        switch j
            case 1 % controle 1
                ctrl = 1;
                ctrl_s = '1';
                alpha = 1;
                if i == 1
                    K = 8.85;
                elseif i == 2
                    K = 24.43;
                else
                    K = 14.4;
                end
            case 2 % controle 2a
                ctrl = -1;
                ctrl_s = '2a';
                alpha = 0;
                if i == 1
                    K = 175;
                elseif i == 2
                    K = 164;
                else
                    K = 215;
                end
            case 3 % controle 2b
                ctrl = -1;
                ctrl_s = '2b';
                alpha = 1;
                if i == 1
                    K = 175;
                elseif i == 2
                    K = 164;
                else
                    K = 215;
                end
        end
        
        sim('ex1sim');
        
        if plot_figures
            figure(1);
            close(1);
            %figure(1);
            %jFrame = get(handle(figure(1)), 'JavaFrame');
            %pause(1);
            %jFrame.setMaximized(1);
            %antro.plot(q0', 'floorlevel', -0.1, 'noname', 'notiles', 'trail', 'b', 'delay', 0, 'noraise')
            %sq = size(q.signals.values);
            %for k = 1:sq(3)
            %   antro.animate(q.Data(:,:,k)');
            %end


            % preparando plots
            stime = size(x.time);
            xplot = zeros(1, stime(1));
            yplot = zeros(1, stime(1));
            zplot = zeros(1, stime(1));

            % plot x
            plot3(xd.signals.values(:,1)', xd.signals.values(:,2)', xd.signals.values(:,3)', 'r');
            hold on;
            for k = 1:stime(1)
                xplot(k) = x.signals.values(1, 4, k);
                yplot(k) = x.signals.values(2, 4, k);
                zplot(k) = x.signals.values(3, 4, k);
            end
            plot3(xplot, yplot, zplot);
            plot3(xd.signals.values(:,1)', xd.signals.values(:,2)', xd.signals.values(:,3)', 'r');
            grid on;
            xlabel('x'); ylabel('y'); zlabel('z');
            hold off;
            legend({'$x_d$', '$x$'},'Interpreter','latex');

            if traj_num == 2
                axis manual;
                hold on;
                [X,Y] = meshgrid(-10:.1:10);
                a=1; b=-1; c=1; d=(0.228 + 0.278);
                Z=(d- a * X - b * Y)/c;
                Color = 1*ones(size(X));
                surf(X,Y,Z,Color, 'LineStyle', 'none', 'FaceAlpha', 0.5, 'EdgeAlpha', 0.5)
                xlabel('x'); ylabel('y'); zlabel('z');
                hold off;
                if save_figure
                    export_fig(['latex/figs/ex1_' traj_s ctrl_s '_x'], '-png', '-opengl');
                end
            elseif save_figure
                export_fig(['latex/figs/ex1_' traj_s ctrl_s '_x'], '-pdf', '-painters', '-transparent');
            end
            axis auto;
        
            % plot erro
            x1plot = zeros(1, stime(1));
            x2plot = zeros(1, stime(1));
            x3plot = zeros(1, stime(1));
            for k = 1:stime(1)
                x1plot(k) = e.signals.values(1, 1, k);
                x2plot(k) = e.signals.values(2, 1, k);
                x3plot(k) = e.signals.values(3, 1, k);
            end
            plot(e.time, x1plot, e.time, x2plot, e.time, x3plot);
            xlabel('time (s)'); ylabel('e (m)');
            legend('e_1', 'e_2', 'e_3');
            if save_figure
                export_fig(['latex/figs/ex1_' traj_s ctrl_s '_e'], '-pdf', '-painters', '-transparent');
            end

            % plot dq
            dq1plot = zeros(1, stime(1));
            dq2plot = zeros(1, stime(1));
            dq3plot = zeros(1, stime(1));
            for k = 1:stime(1)
                dq1plot(k) = dq.signals.values(1, 1, k);
                dq2plot(k) = dq.signals.values(2, 1, k);
                dq3plot(k) = dq.signals.values(3, 1, k);
            end
            plot(e.time, dq1plot, e.time, dq2plot, e.time, dq3plot);
            xlabel('time (s)'); ylabel('u = $\dot{\theta}$','Interpreter','latex');
            legend({'$\dot{\theta}_1$', '$\dot{\theta}_2$', '$\dot{\theta}_3$'},'Interpreter','latex');
            
            if save_figure
                export_fig(['latex/figs/ex1_' traj_s ctrl_s '_dq'], '-pdf', '-painters', '-transparent');
            end
        end
    end
end


