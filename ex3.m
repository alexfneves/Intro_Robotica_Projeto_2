clear;

% % confirmando por Lyapunov
% qv = sym('qv', [3 1], 'real');
% syms q0 K real;
% 
% q = [q0; qv];
% 
% dq = 0.5*[-qv' ; q0*eye(3) - hat(qv)]*K*qv;
% dq0 = dq(1);
% dqv = dq(2:4);
% 
% %dV = dq0 + qev'*dqev
% dV = 2*(1 - q0)*(-dq0) + 2*qv'*dqv
% 
% return;

t_0 = 2.4648;
w_0 = [0.4896 0.2032 0.8480]';
q_0 = [cos(t_0/2); sin(t_0/2)*w_0];

save_figure = true;
plot_figures = true;

for i = 1:4
    if i == 1
        qd0 = 1;
        K = -1;
        ctrl_s = 'a';
    elseif i == 2
        qd0 = 1;
        K = -2;
        ctrl_s = 'b';
    elseif i == 3
        qd0 = -1;
        K = 1;
        ctrl_s = 'c';
    elseif i == 4
        qd0 = -1;
        K = 2;
        ctrl_s = 'd';
    end
    
    sim('ex3sim');
    
    if plot_figures
        
        % plot q
        qplot = q.signals.values;
        plot(q.time, qplot(:,1)', q.time, qplot(:,2)', q.time, qplot(:,3)', q.time, qplot(:,4)');
        xlabel('time (s)'); ylabel('$q$','Interpreter','latex');
        legend({'$q_1$', '$q_2$', '$q_3$', '$q_4$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex3_' ctrl_s '_q'], '-pdf', '-painters', '-transparent');
        end
        
        % plot dq
        dqplot = dq.signals.values;
        plot(dq.time, dqplot(:,1)', dq.time, dqplot(:,2)', dq.time, dqplot(:,3)', dq.time, dqplot(:,4)');
        xlabel('time (s)'); ylabel('$\dot{q}$','Interpreter','latex');
        legend({'$\dot{q}_1$', '$\dot{q}_2$', '$\dot{q}_3$', '$\dot{q}_4$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex3_' ctrl_s '_dq'], '-pdf', '-painters', '-transparent');
        end
        
        % plot V
        V = (qd0 - qplot(:,1)).^2 + qplot(:,2).^2 + qplot(:,3).^2 + qplot(:,4).^2;
        dV = K*qd0*(qplot(:,2).^2 + qplot(:,3).^2 + qplot(:,4).^2);
        plot(q.time, V, q.time, dV);
        xlabel('time (s)');
        legend({'$V$', '$\dot{V}$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex3_' ctrl_s '_Vs'], '-pdf', '-painters', '-transparent');
        end
    end
end