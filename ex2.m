clear;

save_figure = true;
plot_figures = true;

q0 = [pi -pi/2 -pi/2]';

%             th    d       a         alpha      type  offset    mdh
L(1) = Link([ 0     0      0.5          0          0     0],  'standard');
L(2) = Link([ 0 	0      0.5          0          0     0],  'standard');
L(3) = Link([ 0     0	   0.5          0          0     0],  'standard');

planar = SerialLink(L, 'name', 'Planar (3R)', 'comment', 'Alex Fernandes Neves');

% trajetória
syms t real;
xd = [0.25*(1 - cos(pi*t)) 0.25*(2 + sin(pi*t)) 0 0 0 sin(t*pi/24)]';
dxd = diff(xd, t);
matlabFunction(xd, 'File', 'xdFunc');
matlabFunction(dxd,  'File', 'dxdFunc');

% manipulabilidade
syms t1 t2 t3 real;
manip = 0.5*(sin(t2)^2 + sin(t3)^2);
manipParc = [diff(manip, t1); diff(manip, t2); diff(manip, t3)];
matlabFunction(manip, manipParc, 'File', 'manipFunc','Vars', [t1 t2 t3]);

% limite das juntas
tm = [-2*pi -pi/2  -3*pi/2];
tM = [2*pi pi/2 -pi/2];
limJun = 0;
ts = [t1 t2 t3];
n = 3;
for i = 1:n
    limJun = limJun + ((ts(i) - (tM(i) + tm(i))/2)/(tM(i) - tm(i)))^2;
end
limJun = -limJun/(2*n);
limJunParc = [diff(limJun, t1); diff(limJun, t2); diff(limJun, t3)];
matlabFunction(limJun, limJunParc, 'File', 'limJunFunc','Vars', [t1 t2 t3]);

% Jacobiano e trajetória (DESNECESSÁRIO PORQUE NO ENUNCIADO det(J(θ)J T (θ)) é definido)
% syms t1 t2 t3 real;
% x = [1 0 0]';
% y = [0 1 0]';
% z = [0 0 1]';
% 
% h1 = z;
% h2 = z;
% h3 = z;
% 
% p01 = 0;
% p12 = 0.5*x;
% p23 = 0.5*x;
% p43 = 0.5*x;
% 
% R01 = rotz(t1);
% R12 = rotz(t2);
% R23 = rotz(t3);
% R43 = eye(3);
% 
% R02 = rotz(t1 + t2);
% R03 = rotz(t1 + t2 + t3);
% J = [cross(h1, R01*p12 + R02*p23 + R03*p43) cross(R01*h2, R02*p23 + R03*p43) cross(R01*h2, R03*p43); R01*h1 R02*h2 R03*h3];
% J = simplify(J);
% matlabFunction(J, 'File', 'JFunc'); % testado comparando com planar.jacob0(q) para alguns q (erro de q em 1e-15)

% plot q0
if plot_figures
    figure(1);
    close(1);
    planar.plot([0 0 0], 'floorlevel', -0.3, 'noname', 'notiles', 'zoom', 0.6);
    if save_figure
        export_fig(['latex/figs/ex2_planar_0'], '-pdf', '-painters', '-transparent');
    end
    figure(1);
    close(1);
    planar.plot(q0', 'floorlevel', -0.3, 'noname', 'notiles');
    if save_figure
        export_fig(['latex/figs/ex2_planar_tetha0'], '-pdf', '-painters', '-transparent');
    end
end

for i = 1:8 % controle 1, 2, 2a1, 2a100 e 2b1
    K0 = 0;
    switch i
        case 1 % controle 1
            ctrl = 1;
            ctrl_s = '1';
            K = 1;
        case 2 % controle 2
            ctrl = 2;
            ctrl_s = '2';
            K = 1;
        case 3 % controle 2a1
            ctrl = 3;
            ctrl_s = '2a1';
            K = 1;
            K0 = 1;
        case 4 % controle 2a10
            ctrl = 3;
            ctrl_s = '2a10';
            K = 1;
            K0 = 10;
        case 5 % controle 2a100
            ctrl = 3;
            ctrl_s = '2a100';
            K = 1;
            K0 = 100;
        case 6 % controle 2b1
            ctrl = 4;
            ctrl_s = '2b1';
            K = 1;
            K0 = 1;
        case 7 % controle 2b10
            ctrl = 4;
            ctrl_s = '2b10';
            K = 1;
            K0 = 10;
        case 8 % controle 2b100
            ctrl = 4;
            ctrl_s = '2b100';
            K = 1;
            K0 = 100;
    end
    
    sim('ex2sim');
    
    if plot_figures
        figure(1);
        close(1);
        
        % preparando plots
        stime = size(x.time);
        xplot = zeros(1, stime(1));
        yplot = zeros(1, stime(1));
        zplot = zeros(1, stime(1));
        
        % plot trajetória
        planar.plot(q.signals.values(:,:,1)', 'floorlevel', -0.3, 'noname', 'notiles', 'trail', 'b');
        sq = size(q.signals.values);
        for j = 1:sq(3)
          planar.animate(q.signals.values(:,:,j)');
        end
        if save_figure
            export_fig(['latex/figs/ex2_' ctrl_s '_traj'], '-pdf', '-painters', '-transparent');
        end
        
        % plot erro
        x1plot = zeros(1, stime(1));
        x2plot = zeros(1, stime(1));
        x3plot = zeros(1, stime(1));
        if ctrl == 1
            x6plot = zeros(1, stime(1));
        end
        for j = 1:stime(1)
            x1plot(j) = e.signals.values(1, 1, j);
            x2plot(j) = e.signals.values(2, 1, j);
            x3plot(j) = e.signals.values(3, 1, j);
            if ctrl == 1
            x3plot(j) = e.signals.values(6, 1, j);
            end
        end
        if ctrl == 1
            plot(e.time, x1plot, e.time, x2plot, e.time, x6plot, e.time, x6plot);
            legend('e x_1 (m)', 'e x_2 (m)', 'e x_3 (m)', 'e x_6 (rad)');
        else
            plot(e.time, x1plot, e.time, x2plot, e.time, x3plot);
            legend('e x_1 (m)', 'e x_2 (m)', 'e x_3 (m)');
        end
        xlabel('time (s)'); ylabel('erro');
        if save_figure
            export_fig(['latex/figs/ex2_' ctrl_s '_e'], '-pdf', '-painters', '-transparent');
        end
        
        % plot q
        q1plot = zeros(1, stime(1));
        q2plot = zeros(1, stime(1));
        q3plot = zeros(1, stime(1));
        for k = 1:stime(1)
            q1plot(k) = q.signals.values(1, 1, k);
            q2plot(k) = q.signals.values(2, 1, k);
            q3plot(k) = q.signals.values(3, 1, k);
        end
        plot(e.time, q1plot, e.time, q2plot, e.time, q3plot);
        xlabel('time (s)'); ylabel('u = $\dot{\theta}$','Interpreter','latex');
        legend({'$\theta_1$', '$\theta_2$', '$\theta_3$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex2_' ctrl_s '_q'], '-pdf', '-painters', '-transparent');
        end
        
        % plot dq
        dq1plot = zeros(1, stime(1));
        dq2plot = zeros(1, stime(1));
        dq3plot = zeros(1, stime(1));
        for k = 1:stime(1)
            dq1plot(k) = dq.signals.values(1, 1, k);
            dq2plot(k) = dq.signals.values(2, 1, k);
            dq3plot(k) = dq.signals.values(3, 1, k);
        end
        plot(e.time, dq1plot, e.time, dq2plot, e.time, dq3plot);
        xlabel('time (s)'); ylabel('u = $\dot{\theta}$','Interpreter','latex');
        legend({'$\dot{\theta}_1$', '$\dot{\theta}_2$', '$\dot{\theta}_3$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex2_' ctrl_s '_dq'], '-pdf', '-painters', '-transparent');
        end
        
        % plot manip
        if i < 6
            plot(e.time, manip.signals.values);
            xlabel('time (s)'); ylabel('$\omega(\theta)$','Interpreter','latex');
            if save_figure
                export_fig(['latex/figs/ex2_' ctrl_s '_manip'], '-pdf', '-painters', '-transparent');
            end
        end
        
        % plot limJun
        if (i >= 6) || (i == 2)
            plot(e.time, limJun.signals.values);
            xlabel('time (s)'); ylabel('$\omega(\theta)$','Interpreter','latex');
            if save_figure
                export_fig(['latex/figs/ex2_' ctrl_s '_lim_jun'], '-pdf', '-painters', '-transparent');
            end
        end
    end
end

