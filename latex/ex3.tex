
\clearpage
\section{} % 3

\subsection{Análise do Controle}

Deseja-se controlar a orientação de um corpo rígido controlando sua velocidade angular $\omega$, para que saia de uma rotação inicial $R_0$ para $R_d = I_{3x3}$. O controle deve ser realizado com quaternions. Podemos passar estas rotações para seus respectivos quaternions.

\begin{gather*}
R(t) = e^{\hat{\omega}(t)\theta(t)} \qquad
q(t) = \begin{bmatrix} q_0(t) \\ q_v(t) \end{bmatrix} = \begin{bmatrix} cos(\theta(t)/2) \\ sin(\theta(t)/2)\omega(t) \end{bmatrix} \qquad
q = \begin{bmatrix} q_0 \\ q_v \end{bmatrix} \\
R(t_0) = R_0 \implies q(t_0) = \begin{bmatrix} cos(\theta(t_0)/2) \\ sin(\theta(t_0)/2)\omega(t_0) \end{bmatrix} = \begin{bmatrix} 0.3320 \\ 0.4618 \\ 0.1917 \\ 0.7999 \end{bmatrix}\\
R_d = I_{3x3} \implies q_d(t) = 
\left\{\begin{matrix}
\begin{bmatrix} cos(0/2) \\ sin(0/2)\omega(t) \end{bmatrix} = \begin{bmatrix} 1 \\ 0 \\ 0 \\ 0 \end{bmatrix} \\
\begin{bmatrix} cos(2\pi/2) \\ sin(2\pi/2)\omega(t) \end{bmatrix} = \begin{bmatrix} -1 \\ 0 \\ 0 \\ 0 \end{bmatrix}
\end{matrix}\right.
\end{gather*}

Note que $q_d$ é constante e é o quaternion identidade ou o negativo do mesmo, ou seja, a multiplicação de $q_d$ por qualquer outro quaternion retorna este outro ou o outro com todos os termos negativos, não importando a ordem desta multiplicação.

O enunciado define a derivada do quaternion com o termo $\frac{1}{2}E(q)$, que é equivalente ao Jacobiano de representação de quaternions.

\begin{gather*}
\dot{q} = \begin{bmatrix} \dot{q}_0 \\ \dot{q}_v \end{bmatrix} = \frac{1}{2}\begin{bmatrix} -q_v^T \\ q_0I - \hat{q}_v \end{bmatrix}\omega = J_R(q)\omega
\end{gather*}

Também define-se a variável de controle no sistema como sendo a velocidade angular ($\omega = u$), indicando que não é necessário lidar com o Jacobiano analítico para realizar o controle. A questão pede a análise do controle $u = Kq_v$.

\begin{gather*}
\dot{q} = \begin{bmatrix} \dot{q}_0 \\ \dot{q}_v \end{bmatrix} = \frac{1}{2}\begin{bmatrix} -q_v^T \\ q_0I - \hat{q}_v \end{bmatrix}Kq_v
\end{gather*}

Tentaremos provar a estabilidade deste controle no sistema definido por Lyapunov. É um costume definir funções de Lyapunov como sendo a norma do quadrado do erro, com o erro definido como $e = q_d - q$, garantindo $V > 0$.

\begin{mydef}
Devemos considerar que o erro no Lyapunov pode ser diferente do erro de orientação usado no sinal de controle. No controle o erro não é definido como a subtração das matrizes de rotação por que gera uma matriz fora de SO(3). Ele é definido como $e(t) = R_d(t)^TR(t) = R_d^TR$ (enfoque do corpo), e quando este produto for a identidade a orientação do corpo está na orientação desejada. Este erro pode ser reescrito em quaternion como $q_e = q_d^*q$, já que o produto de rotações equivale ao produto de rotações e a transposição de uma matriz de rotação equivale ao conjugado do quaternion associado. Note que seria possível usar outras formulações de erro como $RR_d^T$ (enfoque inercial).
\end{mydef}

\begin{gather*}
||e|| = \sqrt{(q_{d0} - q_0)^2 + ||q_{dv} - q_v||^2} = \sqrt{(q_{d0} - q_0)^2 + (q_{dv} - q_v)^T(q_{dv} - q_v)} \\
V = ||e||^2 = (q_{d0} - q_0)^2 + (q_{dv} - q_v)^T(q_{dv} - q_v) \\
q_{dv} = \begin{bmatrix} 0 \\ 0 \\ 0 \end{bmatrix} \implies V = (q_{d0} - q_0)^2 + q_v^Tq_v \\
\dot{V} = 2(q_{d0} - q_0)(-\dot{q}_0) + 2q_v^T\dot{q}_v
= ((q_{d0} - q_0)q_v^T + q_v^T(q_0I - \hat{q}_v))\omega = \\
= (q_{d0}q_v^T + q_v^T\hat{q}_v)Kq_v \\
\dot{V} = Kq_{d0}q_v^Tq_v
\end{gather*}

Tem-se $\dot{V} \leq 0$ se $sig(K) = - sig(q_{d0})$. Com isto se $qd = \begin{bmatrix}1 & 0 & 0 & 0\end{bmatrix}^T$ é preciso $K < 0$, e se $qd = \begin{bmatrix}1 & 0 & 0 & 0\end{bmatrix}^T$ é preciso $K > 0$. O ganho K influenciará na velocidade de convergência do erro. Este resultado é interessante, pois indica que para $K \neq 0$ o erro irá para 0, então $Q$ irá para uma das opções desejadas, o que significa que $R$ irá para $R_d$.

\subsection{Simulação}

Foi criado um script \textit{ex3.m} para primeiramente configurar variáveis como a condição inicial do quaternion. Depois o script entra em um loop em que configura o ganho de controle, executa o arquivo do Simulink \textit{ex3sim.slx}, e imprime os gráficos de $q$, $\dot{q}$, $V$ e $\dot{V}$. A tabela \ref{tab:ex3_configs} mostra os ganhos e $q_{d0}$ (usado no cálculo de $V$ e $\dot{V}$) para cada configuração.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth]{figs/ex3_sim.png}
\caption{Ex 3: Simulink do controle por quaternion}
\label{fig:ex3_sim}
\end{figure}

No \textit{ex3sim.slx} (figura \ref{fig:ex3_sim}) vemos que o $dq = \dot{q}$ é calculado pelo bloco "Quaternion Derivative" entra no integrador, que gera $q$. O Controle é calculado com a parcela vetorial do quaternion multiplicado pelo ganho $K$ gerando a velocidade angular, que volta ao primeiro bloco.

\begin{table}[!ht]
\centering
\caption{Ex 3: Configurações dos controles}
\label{tab:ex3_configs}
\begin{tabular}{|c|c|c|c|c|}
\hline
\textbf{Controle} & \textbf{a} & \textbf{b} & \textbf{c} & \textbf{d} \\ \hline
\textbf{$K$}      & -1         & -2         & 1          & 2          \\ \hline
\textbf{$q_{d0}$}   & 1          & 1          & -1         & -1         \\ \hline
\end{tabular}
\end{table}

As figuras \ref{fig:ex3_a}, \ref{fig:ex3_b}, \ref{fig:ex3_c} e \ref{fig:ex3_d} mostram os resultados para os controles a, b, c e d. As conclusões são bem diretas e seguem o esperado. Nos ganhos $K$ negativos o $q$ foi para $q_{d0} = 1$, nos ganhos $K$ positivos o $q$ foi para $q_{d0} = -1$. Quanto maior $|K|$, maior é a velocidade de convergência do algoritmo e maior é a amplitude de $\dot{q}$. Pode-se notar que $q$ mantêm sempre sua norma em 1 pelos gráficos de $q$. A função de Lyapunov para dado $q_{d0}$ é sempre positiva e sua derivada sempre negativa, e ambas caem para 0.

% Controle a
\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_a_q.pdf}
    \caption{Quaternion $q$}
    \label{fig:ex3_a_q}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_a_dq.pdf}
    \caption{Derivada do Quaternion $\dot{q}$}
    \label{fig:ex3_a_dq}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_a_Vs.pdf}
    \caption{Função de Lyapunov $V$ e a derivada $\dot{V}$}
    \label{fig:ex3_a_Vs}
    \end{subfigure}
  \end{minipage}
\caption{Ex 3: Controle a ($K = -1$)}
\label{fig:ex3_a}
\end{figure}

% Controle b
\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_b_q.pdf}
    \caption{Quaternion $q$}
    \label{fig:ex3_b_q}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_b_dq.pdf}
    \caption{Derivada do Quaternion $\dot{q}$}
    \label{fig:ex3_b_dq}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_b_Vs.pdf}
    \caption{Função de Lyapunov $V$ e a derivada $\dot{V}$}
    \label{fig:ex3_b_Vs}
    \end{subfigure}
  \end{minipage}
\caption{Ex 3: Controle b ($K = -2$)}
\label{fig:ex3_b}
\end{figure}

% Controle c
\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_c_q.pdf}
    \caption{Quaternion $q$}
    \label{fig:ex3_c_q}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_c_dq.pdf}
    \caption{Derivada do Quaternion $\dot{q}$}
    \label{fig:ex3_c_dq}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_c_Vs.pdf}
    \caption{Função de Lyapunov $V$ e a derivada $\dot{V}$}
    \label{fig:ex3_c_Vs}
    \end{subfigure}
  \end{minipage}
\caption{Ex 3: Controle c ($K = 1$)}
\label{fig:ex3_c}
\end{figure}

% Controle d
\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_d_q.pdf}
    \caption{Quaternion $q$}
    \label{fig:ex3_d_q}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_d_dq.pdf}
    \caption{Derivada do Quaternion $\dot{q}$}
    \label{fig:ex3_d_dq}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex3_d_Vs.pdf}
    \caption{Função de Lyapunov $V$ e a derivada $\dot{V}$}
    \label{fig:ex3_d_Vs}
    \end{subfigure}
  \end{minipage}
\caption{Ex 3: Controle d ($K = 2$)}
\label{fig:ex3_d}
\end{figure}