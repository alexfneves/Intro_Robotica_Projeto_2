
\clearpage
\section{} % 2

O primeiro passo neste exercício é usar o método do Denavit Hartenberg para que a Robot Toolbox do MATLAB possa ser utilizada no controle cinemático. A figura \ref{fig:ex2_planar_dh} mostra a representação do manipulador planar de 3 juntas de revolução com os eixos escolhidos de acordo com o DH Standard. A tabela \ref{tab:ex2_planar_dh} mostra os parâmetros para o método. Note que a escolha das origens $O_0$ e $O_3$ na figura faz com que a tabela tenha apenas os parâmetros $a_i$ diferente de 0. A figura \ref{fig:ex2_planar_0} mostra o manipulador na Robot Toolbox para juntas zeradas, enquanto \ref{fig:ex2_planar_tetha0} mostra o manipulador na condição inicial da simulação.

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{figs/ex2_planar_dh.png}
\caption{Ex 2: Representação do manipulador planar 3R com o DH Standard}
\label{fig:ex2_planar_dh}
\end{figure}

\begin{table}[!ht]
\centering
\caption{Ex 2: Tabela de parâmetros do Denavit Hartenberg Standard para o manipulador planar 3R}
\label{tab:ex2_planar_dh}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\textbf{Elo i} & \textbf{$\theta_i$} & \textbf{$d_i$} & \textbf{$a_i$} & \textbf{$\alpha_i$} & \textbf{type} & \textbf{offset} \\ \hline
1                & $\theta_1$            & $0$     & $0.5m$         & $0$               & $0$             & $0$           \\ \hline
2                & $\theta_2$            & $0$              & $0.5m$            & $0$                   & $0$             & $0$           \\ \hline
3                & $\theta_3$            & $0$              & $0.5m$         & $0$               & $0$             & $0$               \\ \hline
\end{tabular}
\end{table}

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{figs/ex2_planar_0.pdf}
\caption{Ex 2: Manipulador planar 3R com juntas em 0}
\label{fig:ex2_planar_0}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{figs/ex2_planar_tetha0.pdf}
\caption{Ex 2: Manipulador planar 3R com juntas em $\theta_0 = [\pi -\pi/2 -\pi/2]^T$}
\label{fig:ex2_planar_tetha0}
\end{figure}

\subsection{Controle}

A trajetória do efetuador é descrita com $p_d$ (todos os controles) e $\phi_d$ (controle \textit{1}). Dada a trajetória e o controle cinemático, propõe-se inicialmente o uso do controle proporcional com feedforward da velocidade da trajetória. É preciso derivar a trajetória desejada.

\begin{gather*}
\dot{p}_d = \begin{bmatrix}0.25\pi sin(\pi t) \\ 0.25\pi cos(\pi t)\end{bmatrix} \\
\dot{\phi}_d = \cfrac{\pi cos(\frac{\pi t}{24})}{24}
\end{gather*}

Analisemos agora o manipulador.

\begin{gather*}
\vec{x} = \begin{bmatrix}\vec{v} \\ \vec{\omega}\end{bmatrix} = J_e\dot{\theta} \qquad
(\vec{x})_0 = x = \begin{bmatrix}(\vec{v})_0 \\ (\vec{\omega})_0\end{bmatrix} = \begin{bmatrix}v \\ \omega\end{bmatrix} = (J_e)_0\dot{\theta}
\end{gather*}

$(J_e)_0$ é uma matriz $6x3$, mas se tratando de um manipulador planar, a terceira até a quarta linha de $(J_e)_0$ será uma matriz de zeros, e a sexta linha será uma linha de uns.

\begin{gather*}
(h_1)_0 = (h_2)_0 = (h_3)_0 = \begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix}^T \\
\omega = \begin{bmatrix} \omega_1 \\ \omega_2 \\ \omega_3 \end{bmatrix} = \begin{bmatrix} 0 \\ 0 \\ \dot{\theta}_1 + \dot{\theta}_2 + \dot{\theta}_3 \end{bmatrix} 
\end{gather*}

Normalmente $\omega$ é uma termo que não é utilizado diretamente no controle, sendo necessário escolher uma representação, para então trabalhar com o jacobiano analítico no controle, escolhendo uma referência nesta representação.

\begin{gather*}
\int w_3dt = \theta_1 + \theta_2 + \theta_3 = \phi
\end{gather*}

Podemos ver no caso do manipulador planar 3R que $w_3$ pode ser comparado com a referência $\phi_d$, portanto não precisamos nos preocupar com a representação.

No controle \textit{1} desejamos controlar 3 graus de liberdade com três juntas. Podemos trabalhar com um Jacobiano reduzido retirando as linhas zeradas para que sua inversão seja possível, e trabalhar com $x'$ ($x$ reduzido).

\begin{gather*}
x' = \begin{bmatrix} p_1 \\ p_2 \\ \phi \end{bmatrix} \qquad
x'_d = \begin{bmatrix} p_{1d} \\ p_{2d} \\ \phi_d \end{bmatrix} 
\end{gather*}
\begin{gather}\label{eq:ex2_controle1}
u = (J_{126})_0^{-1}\bar{u} = (J_{126})_0^{-1}(\dot{x'}_d + K(x'_d - x'))
\end{gather}

No controle \textit{2} desejamos controlar 2 graus de liberdade tendo 3 juntas disponíveis. Por isto o controle de trajetória será feito com a pseudo inversa do Jacobiano reduzido (apenas linhas 1 e 2 desta vez).

\begin{gather*}
x' = \begin{bmatrix} p_1 \\ p_2 \end{bmatrix} \quad
x'_d = \begin{bmatrix} p_{1d} \\ p_{2d} \end{bmatrix} \\
u = (J_{12})_0^{\dagger}\bar{u} = (J_{12})_0^{\dagger}(\dot{x'}_d + K(x'_d - x'))
\end{gather*}

Como queremos aproveitar o espaço nulo podemos adicionar um termo extra que não influenciará na trajetória.

\begin{gather}\label{eq:ex2_controle2}
u = (J_{12})_0^{\dagger}(\dot{x'}_d + K(x'_d - x')) + (I - (J_{12})_0^{\dagger}(J_{12})_0)\mu
\end{gather}

O $\mu$ é um termo que pode ser usado para influenciar as juntas dado uma certa função objetivo $w(\theta) > 0$.

\begin{gather}\label{eq:ex2_w}
w(\theta) = K_0\left(\cfrac{\partial w}{\partial\theta}\right)^T
\end{gather}

O enunciado pede a minimização da manipulabilidade e do limite das juntas. A equação \ref{eq:ex2_w_1} mostra a função objetivo da manipulabilidade, e a equação \ref{eq:ex2_w_2} mostra a função objetivo dos limites das juntas.

\begin{gather}\label{eq:ex2_w_1}
w_1(\theta) = det((J_{12})_0(J_{12})_0^T) = 0.5(sin(\theta_2)^2 + sin(\theta_3)^2)
\end{gather}

\begin{gather*}
\theta_{im} \leq \theta_i \leq \theta_{iM} \quad
w_2(\theta) = -\cfrac{1}{2*3}\displaystyle\sum_{i = 1}^3 \left(\cfrac{\theta_i - \bar{\theta}_i}{\theta_{iM} - \theta_{im}}\right)^2
\end{gather*}

\begin{gather}\label{eq:ex2_w_2}
w_2(\theta) = - \left(\cfrac{\theta_1^2}{96\pi^2} + \cfrac{\theta_2^2}{6\pi^2} + \cfrac{(\pi + \theta_3)^2}{6\pi^2}\right)
\end{gather}

O cálculo de $\mu$ no controle exige a derivação parcial destas funções (equações \ref{eq:ex2_dw_1} e \ref{eq:ex2_dw_2})

\begin{gather}\label{eq:ex2_dw_1}
\cfrac{\partial w_1}{\partial\theta} = \begin{bmatrix} 0 & cos(\theta_2)sin(\theta_2) & cos(\theta_3)sin(\theta_3)\end{bmatrix}
\end{gather}

\begin{gather}\label{eq:ex2_dw_2}
\cfrac{\partial w_2}{\partial\theta} = \begin{bmatrix}-\cfrac{\theta_1}{48\pi^2} & -\cfrac{\theta_2}{3\pi^2} & -\cfrac{2\pi + 2\theta_3}{6\pi^2} \end{bmatrix}
\end{gather}


\subsection{Simulação}

O script \textit{ex2.m} e o arquivo simulink \textit{ex2sim.m} foram criados para simular os controles. O script cria o manipulador planar, imprime a configuração de juntas zeradas e do inicio da simulação, define a trajetória desejada, deriva a trajetória desejada. Em seguida o script entra em um loop em que variáveis de configuração do \textit{ex2sim.m} são definidas para cada tipo de controle e este é então executado, para depois imprimir várias variáveis de interesse. A trajetória, manipulabilidade e o limite das juntas foram transformadas em funções para serem usadas no bloco Matlab Function do Simulink.

A figura \ref{fig:ex2_sim_prop_feedforward} mostra o modelo no simulink. O sistema é representado pelo integrador (condição inicial descrita no enunciado), que recebe a derivada das juntas e devolve o o valor das juntas. $q$ equivale a $\theta$. O sinal $x$ é construído através da cinemática direta e da soma das 3 juntas ($\phi$). Com a trajetória gera-se $\dot{x}_d + K(x_d - x)$, similar aos $\bar{u}$ das equações \ref{eq:ex2_controle1} e \ref{eq:ex2_controle2}. Este sinal junto de $q$ e do $(J_e)_0$ são passados ao bloco Método de Controle. A figura \ref{fig:ex2_sim_metodo_controle} mostra este bloco, que é responsável por ativar o controle desejado na simulação. O Controle 1 (figura \ref{fig:ex2_sim_controles_controle1}) computa o controle da equação \ref{eq:ex2_controle1} e o Controle 2 (figura \ref{fig:ex2_sim_controles_controle2}) computa o controle das equações \ref{eq:ex2_controle2} e \ref{eq:ex2_w}. O Controle 2 usa as derivadas parciais (equações \ref{eq:ex2_dw_1} e \ref{eq:ex2_dw_2}) dos blocos Controle 2a (figura \ref{fig:ex2_sim_controles_manip}) e 2b (figura \ref{fig:ex2_sim_controles_lim_jun}), que também geram $w_1$ e $w_2$ (equações \ref{eq:ex2_w_1} e \ref{eq:ex2_w_2}).

\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_sim.png}
    \caption{Manipulador com controle proporcional e feedforward}
    \label{fig:ex2_sim_prop_feedforward}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_sim_metodo_controle.png}
    \caption{Métodos de controle escolhidos (dq é calculado por apenas 1 caminho por vez)}
    \label{fig:ex2_sim_metodo_controle}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Simulink da simulação de controle do manipulador planar 3R}
\label{fig:ex2_sim}
\end{figure}

\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_sim_controle1.png}
    \caption{Cálculo do Controle com a inversa do Jacobiano}
    \label{fig:ex2_sim_controles_controle1}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.6\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_sim_controle2.png}
    \caption{Cálculo do Controle com a pseudo inversa do Jacobiano aproveitando o espaço nulo do Jacobiano}
    \label{fig:ex2_sim_controles_controle2}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_sim_manip.png}
    \caption{Cálculo da manipulabilidade e derivadas parciais}
    \label{fig:ex2_sim_controles_manip}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_sim_lim_jun.png}
    \caption{Cálculo dos limites das juntas e derivadas parciais}
    \label{fig:ex2_sim_controles_lim_jun}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controles no Simulink}
\label{fig:ex2_sim_controles}
\end{figure}

\clearpage

\subsection{Resultados}

O script foi executado para realizar os Controles 1, 2a e 2b. Uso-se o ganho $K = 1$, e nos Controles 2a e 2b os ganhos $K_0 = 1, 10, 100$. Os resultados estão nas figuras \ref{fig:ex2_1}, \ref{fig:ex2_2}, \ref{fig:ex2_2a1}, \ref{fig:ex2_2a10}, \ref{fig:ex2_2a100}, \ref{fig:ex2_2b1}, \ref{fig:ex2_2b10} e \ref{fig:ex2_2b100}.

A primeira observação que pode ser feita é que em todos os casos o erro é virtualmente 0 em todos os controles testados. No Controle 1 há também o erro na orientação em radianos.

O controle começa em 0 porque a condição inicial do manipulador dado no enunciado está na trajetória desejada ($p_d$ e $\phi_d$). O termo feedforward no controle se encarrega de manter o efetuador na trajetória desejada em cada grau de liberdade. Por este motivo o ganho $K$ não importa nesta simulação. No entanto se o manipulador começasse de outra condição inicial, encontraríamos um erro que decresce exponencialmente para 0.

Vale ressaltar a sagacidade da escolha do tempo de simulação de $t \in [0,4]$ no caso do Controle 1. O $\phi_d$ segue uma senoide, e fora deste intervalo o Jacobiano fica mal condicionado em vários momentos. Isto é uma reação da dificuldade do manipulador conciliar os objetivos $\phi_d$ e $p_d$.

Apesar do erro 0 em todos, as juntas se comportam de forma bem diferente em cada controle. Podemos ver isto pelos gráficos das juntas e pelos gráficos da trajetória (o manipulador se encontra em sua última configuração até o fim da simulação).

Para analisar o Controle 2a, é preciso relembrar a manipulabilidade. O termo se refere a um índice usado para analisar os valores singulares do Jacobiano $\sigma_i$, que por sua vez podem ser vistos no elipsoide de manipulabilidade. No controle cinemático desejamos que estes $\sigma_i$ estejam longe de 0, pois isto significaria que nenhum sinal de controle seria capaz de mover o efetuador no grau de liberdade associado a $\sigma_i$. Em teoria o Controle 2a fará com que estes $\sigma_i$ se afastem de 0. A função da manipulabilidade escolhida equivale ao quadrado da multiplicação dos $\sigma_i$. Pela fórmula de $w_1(\theta)$ aproximada a manipulabilidade pode se encontrar entre 0 ($\theta_1 = \theta_2 = \alpha\pi$) e 1 ($\theta_1 = \theta_2 = \pm\alpha\pi/2$).

Analisando o gráfico da manipulabilidade no Controle 1 (figura \ref{fig:ex2_1_manip}) nota-se que em vários momentos a manipulabilidade chega próximo de 0 (entre 0.03 e 1). No Controle 2 (sem aproveitar o espaço nulo) (figura \ref{fig:ex2_2_manip}) a manipulabilidade é bem melhor, ficando na faixa de 0.85 e 1 (muito melhor), o que foi possível ao não restringir a orientação do manipulador. No Controle 2a com $K_0 = 1$ (figura \ref{fig:ex2_2a1_manip}) pode-se observar uma leve melhora em relação ao Controle 2 com mínimo em 0.88, sendo mais aparente em $t = 1.4$. Com $K_0 = 10$ (figura \ref{fig:ex2_2a10_manip}) o mínimo passa a 0.9 e todos os cumes chegam a 1. Com $K_0 = 100$ (figura \ref{fig:ex2_2a100_manip}) notamos os vales de cima no gráfico ficarem mais perto de 1. Um resultado interessante é que ao olhar os gráficos de posição e velocidade das juntas notamos que quanto maior o ganho, mais $\theta_2$ se aproxima de $\theta_3$. Isto faz sentido já que a $w_1$ é a soma de senoides das duas variáveis. Resumindo, quanto maior $K_0$, melhor fica a manipulabilidade.

Mudemos agora o foco para Controle 2b com o limite das juntas. Este controle deve tentar levar $w_2$ para 0 dado que o efetuador ainda siga a trajetória desejada, tentando forçar cada junta a ir para a média dos seus limites ($\theta_1$ e $\theta_2$ em $0$ e $\theta_3$ em $-\pi/2$). O Controle 2 faz com que as juntas sigam um padrão periódico como no sinal de referência (período de 2 segundos). No Controle 2b com $K = 1$ (figura \ref{fig:ex2_2b1_lim_jun}) é possível ver uma distorção deste padrão, com o vale de $w_2$ indo de 0.11 (primeiro período) para 0.08 (segundo ciclo). A diferença é ainda maior em $K = 10$ (figura \ref{fig:ex2_2b10_lim_jun}), com $w_2$ indo para 0. Já possível ver as juntas tentando mantar-se em suas médias (figura \ref{fig:ex2_2b10_q}). Com $K = 100$ (figura \ref{fig:ex2_2b100_lim_jun}) vemos o resultado anterior amplificado, com $w_2$ chegando a 0 em certos momentos e as juntas próximas da média de seus limites (figura \ref{fig:ex2_2b100_q}). No entanto, este ganho é muito alto a ponto de criar descontinuidades e picos no sinal de controle (figura \ref{fig:ex2_2b100_dq}), até trocando o sinal da velocidade das juntas em certos momentos.

O que não pode ser visto nas figuras mas é possível de se enxergar durante a simulação é a colisão que ocorre com os elos. No Controle 1 as juntas/elos não colidem mas chegam perto de colidir. No Controle 2b, apesar de ser possível gerar as trajetórias com os limites, a média dos limites deixa o manipulador em uma posição pouco "confortável". Por exemplo, com $K_0 = 100$ o manipulador fica praticamente esticado ($\theta_2 \approx 0$) com o efetuador "voltando" para a junta 2 ($\theta_3 = -\pi$) em vários momentos.

Resumindo, o Controle 2b consegue atuar no espaço nulo para distanciar as juntas de seus limites, mas o sinal de controle pode ser violento dependendo do ganho $K_0$.

\pagebreak

% Controle 1
\begin{figure}[!ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \centering
    \includegraphics[width=0.6\textwidth]{figs/ex2_1_traj.pdf}
    \caption{Trajetória}
    \label{fig:ex2_1_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_1_q.pdf}
    \caption{Ângulo das juntas}
    \label{fig:ex2_1_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_1_e.pdf}
    \caption{Erro da posição do efetuador}
    \label{fig:ex2_1_e}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_1_dq.pdf}
    \caption{Sinal de controle/velocidade das juntas}
    \label{fig:ex2_1_dq}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_1_manip.pdf}
    \caption{Manipulabilidade}
    \label{fig:ex2_1_manip}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controle 1}
\label{fig:ex2_1}
\end{figure}

% Controle 2 K0 = 0
\begin{figure}[!ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \centering
    \includegraphics[width=0.69\textwidth]{figs/ex2_2_traj.pdf}
    \caption{Trajetória}
    \label{fig:ex2_2_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2_q.pdf}
    \caption{Ângulo das juntas}
    \label{fig:ex2_2_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2_e.pdf}
    \caption{Erro da posição do efetuador}
    \label{fig:ex2_2_e}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2_dq.pdf}
    \caption{Sinal de controle/velocidade das juntas}
    \label{fig:ex2_2_dq}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2_manip.pdf}
    \caption{Manipulabilidade}
    \label{fig:ex2_2_manip}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controle 2 sem parcela que manipula o espaço nulo}
\label{fig:ex2_2}
\end{figure}

% Controle 2a K0 = 1
\begin{figure}[!ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \centering
    \includegraphics[width=0.69\textwidth]{figs/ex2_2a1_traj.pdf}
    \caption{Trajetória}
    \label{fig:ex2_2a1_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a1_q.pdf}
    \caption{Ângulo das juntas}
    \label{fig:ex2_2a1_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a1_e.pdf}
    \caption{Erro da posição do efetuador}
    \label{fig:ex2_2a1_e}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a1_dq.pdf}
    \caption{Sinal de controle/velocidade das juntas}
    \label{fig:ex2_2a1_dq}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a1_manip.pdf}
    \caption{Manipulabilidade}
    \label{fig:ex2_2a1_manip}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controle 2a com $K_0 = 1$}
\label{fig:ex2_2a1}
\end{figure}

% Controle 2a K0 = 10
\begin{figure}[!ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \centering
    \includegraphics[width=0.69\textwidth]{figs/ex2_2a10_traj.pdf}
    \caption{Trajetória}
    \label{fig:ex2_2a10_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a10_q.pdf}
    \caption{Ângulo das juntas}
    \label{fig:ex2_2a10_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a10_e.pdf}
    \caption{Erro da posição do efetuador}
    \label{fig:ex2_2a10_e}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a10_dq.pdf}
    \caption{Sinal de controle/velocidade das juntas}
    \label{fig:ex2_2a10_dq}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a10_manip.pdf}
    \caption{Manipulabilidade}
    \label{fig:ex2_2a10_manip}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controle 2a com $K_0 = 10$}
\label{fig:ex2_2a10}
\end{figure}

% Controle 2a K0 = 10
\begin{figure}[!ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \centering
    \includegraphics[width=0.69\textwidth]{figs/ex2_2a100_traj.pdf}
    \caption{Trajetória}
    \label{fig:ex2_2a100_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a100_q.pdf}
    \caption{Ângulo das juntas}
    \label{fig:ex2_2a100_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a100_e.pdf}
    \caption{Erro da posição do efetuador}
    \label{fig:ex2_2a100_e}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a100_dq.pdf}
    \caption{Sinal de controle/velocidade das juntas}
    \label{fig:ex2_2a100_dq}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2a100_manip.pdf}
    \caption{Manipulabilidade}
    \label{fig:ex2_2a100_manip}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controle 2a com $K_0 = 100$}
\label{fig:ex2_2a100}
\end{figure}

% Controle 2b K0 = 1
\begin{figure}[!ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \centering
    \includegraphics[width=0.69\textwidth]{figs/ex2_2b1_traj.pdf}
    \caption{Trajetória}
    \label{fig:ex2_2b1_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b1_q.pdf}
    \caption{Ângulo das juntas}
    \label{fig:ex2_2b1_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b1_e.pdf}
    \caption{Erro da posição do efetuador}
    \label{fig:ex2_2b1_e}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b1_dq.pdf}
    \caption{Sinal de controle/velocidade das juntas}
    \label{fig:ex2_2b1_dq}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b1_lim_jun.pdf}
    \caption{Limite das juntas}
    \label{fig:ex2_2b1_lim_jun}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controle 2b com $K_0 = 1$}
\label{fig:ex2_2b1}
\end{figure}

% Controle 2b K0 = 10
\begin{figure}[!ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \centering
    \includegraphics[width=0.69\textwidth]{figs/ex2_2b10_traj.pdf}
    \caption{Trajetória}
    \label{fig:ex2_2b10_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b10_q.pdf}
    \caption{Ângulo das juntas}
    \label{fig:ex2_2b10_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b10_e.pdf}
    \caption{Erro da posição do efetuador}
    \label{fig:ex2_2b10_e}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b10_dq.pdf}
    \caption{Sinal de controle/velocidade das juntas}
    \label{fig:ex2_2b10_dq}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b10_lim_jun.pdf}
    \caption{Limite das juntas}
    \label{fig:ex2_2b10_lim_jun}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controle 2b com $K_0 = 10$}
\label{fig:ex2_2b10}
\end{figure}

% Controle 2b K0 = 10
\begin{figure}[!ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{1\textwidth}
    \centering
    \includegraphics[width=0.69\textwidth]{figs/ex2_2b100_traj.pdf}
    \caption{Trajetória}
    \label{fig:ex2_2b100_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b100_q.pdf}
    \caption{Ângulo das juntas}
    \label{fig:ex2_2b100_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b100_e.pdf}
    \caption{Erro da posição do efetuador}
    \label{fig:ex2_2b100_e}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b100_dq.pdf}
    \caption{Sinal de controle/velocidade das juntas}
    \label{fig:ex2_2b100_dq}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex2_2b100_lim_jun.pdf}
    \caption{Limite das juntas}
    \label{fig:ex2_2b100_lim_jun}
    \end{subfigure}
  \end{minipage}
\caption{Ex 2: Controle 2b com $K_0 = 100$}
\label{fig:ex2_2b100}
\end{figure}



