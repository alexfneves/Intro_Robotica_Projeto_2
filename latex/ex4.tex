
\clearpage
\section{} % 4

\subsection{Análise do Controle}

O primeiro passo neste exercício é usar o método do Denavit Hartenberg para que a Robot Toolbox do MATLAB possa ser utilizada no controle cinemático. A figura \ref{fig:ex4_planar_dh} mostra a representação do manipulador planar de 2 juntas de revolução com os eixos escolhidos de acordo com o DH Standard. A tabela \ref{tab:ex4_planar_dh} mostra os parâmetros para o método. Note que a escolha das origens $O_0$ e $O_2$ na figura faz com que a tabela tenha apenas os parâmetros $a_i$ diferente de 0. A figura \ref{fig:ex4_planar_0} mostra o manipulador na Robot Toolbox para juntas zeradas, enquanto \ref{fig:ex4_planar_tetha0} mostra o manipulador na condição inicial da simulação.

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{figs/ex4_planar_dh.png}
\caption{Ex 4: Representação do manipulador planar 2R com o DH Standard}
\label{fig:ex4_planar_dh}
\end{figure}

\begin{table}[!ht]
\centering
\caption{Ex 4: Tabela de parâmetros do Denavit Hartenberg Standard para o manipulador planar 2R}
\label{tab:ex4_planar_dh}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
\textbf{Elo i} & \textbf{$\theta_i$} & \textbf{$d_i$} & \textbf{$a_i$} & \textbf{$\alpha_i$} & \textbf{type} & \textbf{offset} \\ \hline
1                & $\theta_1$            & $0$     & $0.45m$         & $0$               & $0$             & $0$           \\ \hline
2                & $\theta_2$            & $0$              & $0.5m$            & $0$                   & $0$             & $0$           \\ \hline
\end{tabular}
\end{table}

\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.8\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_planar_0.pdf}
    \caption{Juntas em 0}
    \label{fig:ex4_planar_0}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.8\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_planar_tetha0.pdf}
    \caption{Juntas em $\theta_0 = \begin{bmatrix}\pi/20 & -\pi/2\end{bmatrix}^T$}
    \label{fig:ex4_planar_tetha0}
    \end{subfigure}
  \end{minipage}
\caption{Ex 4: Configurações do manipulador planar 2R}
\label{fig:ex4_planar}
\end{figure}

A servo visão é explicada nos slides do capítulo 7, e consiste em representar o manipulador na imagem que é capturada por uma câmera. Usaremos uma nomenclatura diferente da formulação do DH, sendo $(\vec{x})_0 = x = \begin{bmatrix}x_1 & x_2\end{bmatrix}^T$ a posição do efetuador no mundo, $(\vec{x})_{imagem} = x_c = \begin{bmatrix}x_{c1} & x_{c2}\end{bmatrix}^T$ a posição do efetuador na imagem da câmera. Assume-se $x, x_c, x_{c0} \in \mathbb{R}^{2x2}$, já que trata-se de um manipulador planar e que a câmera está perpendicular ao plano de trabalho do manipulador. A figura \ref{fig:ex4_planar_tetha0} mostra o manipulador na posição inicial com o eixo que representa a câmera $O_{camera}$.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figs/ex4_planar_tetha0_camera.pdf}
\caption{Ex 4: Frames inercial, do efetuador (manipulador em $\theta_0$) e da câmera}
\label{fig:ex4_planar_tetha0_camera}
\end{figure}

\begin{gather*}
f = 8mm \quad
z_0 = 0.64m \quad
\alpha = 72727pixels/m \quad
O_c = \begin{bmatrix}320 \\ 240\end{bmatrix} \quad
p_{bc} = \begin{bmatrix}0.5 \\ -0.38\end{bmatrix} \\
K_p = \cfrac{f\alpha}{z_0}\begin{bmatrix}1 & 0 \\ 0 & -1\end{bmatrix}\begin{bmatrix}c_\phi & s_\phi \\ -s_\phi & c_\phi\end{bmatrix} \\
x_{c0} = -K_pp_{bc} + O_c \\
x_c = K_px + x_{c0}
\end{gather*}

Assume-se que a distorção $\alpha$ é a mesma nos dois eixos da imagem. Note que $K_p$ consiste na conversão de metros para pixels e de duas rotações seguidas no sistema de coordenadas corrente, a primeira de $\pi$ no eixo $\vec{x}$ e a segunda de $\phi$ em $\vec{z}$.

O controle proposto consiste em representar o manipulador na imagem da câmera e usar um sinal de referência do efetuador na imagem para gerar um erro. Um controle proporcional com feedforward usando a inversa do Jacobiano é o suficiente para levar o sistema até a trajetória desejada, mas é preciso atentar que o Jacobiano do efetuador na imagem equivale a $(J_c)_c = J_c$.

\begin{gather}
(J_c(\theta))_c = K_p(J(\theta))_0 \\
\label{eq:ex4_x}
\dot{x}_c = K_p\dot{x} = K_p(J)_0u = (J_c)_cu = J_cu \\
\label{eq:ex4_x_c}
u = J_c^{-1}\bar{u} = J_c^{-1}(\dot{x}_{cd} + K(x_{cd} - x_c)) \\
e_c = x_{cd} - x_c \qquad
\dot{e}_c = \dot{x}_{cd} - \dot{x}_c = \dot{x}_{cd} - J_cJ_c^{-1}(\dot{x}_{cd} + K(x_{cd} - x_c)) \\
\dot{e_c} = -Ke_c
\end{gather}

K é o ganho de controle que basta satisfazer $K > 0$ para que o sistema fique estável, contanto que $J$ não passe por singularidades. A trajetória desejada é chamada de $x_{cd}$ e é dado no enunciado com $w_n = 1$:

\begin{gather*}
x_{cd} = \begin{bmatrix} 300 + 70 (sin(w_nt) + sin(1.5w_nt)) \\ 200 + 70 (sin(w_nt + 1.6) + sin(1.5w_nt + 1.6))\end{bmatrix} \\
\dot{x}_{cd} = \begin{bmatrix} 70w_ncos(w_nt) + 105w_ncos(1.5w_nt) \\
70w_ncos(w_nt + 1.6) + 105w_ncos(1.5t + 1.6)\end{bmatrix}
\end{gather*}


\subsection{Simulação}

A simulação foi feita com o script \textit{ex4.m} e \textit{ex4sim.slx}. O script gera o manipulador na Robot Toolbox com o DH e gera as figuras com as posições iniciais do manipulador. Depois define a trajetória desejada e sua derivada e entra em um loop em que a única coisa que muda é a rotação da câmera por $\phi$. No loop calcula-se $K_p$ e $x_{c0}$, executa-se \textit{ex4sim.slx} e imprime os gráficos mais importantes.

O \textit{ex4sim.slx} (figura \ref{fig:ex4_sim}) equivale a uma simples malha de controle proporcional descrita nas equações \ref{eq:ex4_x} e \ref{eq:ex4_x_c}. Note que os atuadores do manipulador são representados pelo integrador com condição inicial $\theta_0$, e $q$ equivale ao $\theta$ dos atuadores. As submatrizes retiram $x$ e $J$ do resultado dos blocos jacob0 e fkine da Robot Toolbox ($T \in \mathbb{R}^{4x4}$ e $(J_e)_0 \in \mathbb{R}^{6x2}$). $x$ e $J$ são transformados em $x_c$ e $J_c$ ao serem multiplicados por $K_p$. O bloco Trajectory gera a trajetória desejada e sua derivada a cada instante de integração.

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{figs/ex4_sim.pdf}
\caption{Ex 4: Simulink do controle}
\label{fig:ex4_sim}
\end{figure}

\subsection{Resultados}

Podemos ver o resultado da simulação para $\phi = 0$ na figura \ref{fig:ex4_1} e para $\phi = \pi/4$ na figura \ref{fig:ex4_2}. As figuras de trajetórias ignoraram eixo $z$. Podemos verificar que em ambos os casos (figuras \ref{fig:ex4_1_e_c} e \ref{fig:ex4_2_e_c}) que o erro chega a 0 após 5 segundos de simulação. As figuras \ref{fig:ex4_1_x_c} e \ref{fig:ex4_2_x_c} são muito parecidas, mas podemos verificar que ambas possuem condições iniciais diferentes, e que eventualmente o efetuador gera a trajetória desejada $x_cd$. A condição inicial pode ser melhor entendida comparando as trajetórias (figuras \ref{fig:ex4_1_traj} e \ref{fig:ex4_2_traj}). Vemos também na \ref{fig:ex4_1_traj} que o desenho está invertido no eixo $x_2$, devido à rotação no eixo $x_1$ do $K_p$, e na \ref{fig:ex4_2_traj} notamos que a imagem ainda é rotacionada pelo $\phi = \phi/4$. Não há muito o que concluir sobre a posição e velocidade das juntas além de que são muito parecidos e que o sinal de controle não é muito grande pela posição inicial do manipulador estar próxima do desejado.

\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_1_traj.pdf}
    \caption{Trajetória do manipulador}
    \label{fig:ex4_1_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_1_x_c.pdf}
    \caption{Trajetórias $x_{cd}$ e $x_c$}
    \label{fig:ex4_1_x_c}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_1_e_c.pdf}
    \caption{Erro $e_{c} = x_{cd} - x_c$}
    \label{fig:ex4_1_e_c}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_1_q.pdf}
    \caption{Posição das juntas $\theta$}
    \label{fig:ex4_1_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_1_dq.pdf}
    \caption{Sinal de controle $u = \dot{\theta}$.}
    \label{fig:ex4_1_dq}
    \end{subfigure}
  \end{minipage}
\caption{Ex 4: Resultado da simulação para $\phi = 0$.}
\label{fig:ex4_1}
\end{figure}

\begin{figure}[ht]
\centering
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.55\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_2_traj.pdf}
    \caption{Trajetória do manipulador}
    \label{fig:ex4_2_traj}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_2_x_c.pdf}
    \caption{Trajetórias $x_{cd}$ e $x_c$}
    \label{fig:ex4_2_x_c}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_2_e_c.pdf}
    \caption{Erro $e_{c} = x_{cd} - x_c$}
    \label{fig:ex4_2_e_c}
    \end{subfigure}
  \end{minipage}
  \begin{minipage}{\linewidth}
  \centering
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_2_q.pdf}
    \caption{Posição das juntas $\theta$}
    \label{fig:ex4_2_q}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=1\textwidth]{figs/ex4_2_dq.pdf}
    \caption{Sinal de controle $u = \dot{\theta}$.}
    \label{fig:ex4_2_dq}
    \end{subfigure}
  \end{minipage}
\caption{Ex 4: Resultado da simulação para $\phi = \pi/4$.}
\label{fig:ex4_2}
\end{figure}

O ganho $K$ não foi estudado neste problema (simulações repetidas variando $K$) pois sua variação já foi abordada nos outros exercícios deste projeto. O ganho influencia na velocidade de convergência do erro.
